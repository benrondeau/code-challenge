jQuery('#submit-btn').on('click', function(){
   
   // Grab content of message
   var messageContent = jQuery('#chat-message').val(),
   
   // Empty object for processed message output to be stored
   json_output = {},
   
   //Temporary storage for any URLs
   URLs = [];
   
   //Regular expressions
   var urlREGEX = new RegExp(/(^|\s)((https?:\/\/)?[\w-]+(\.[\w-]+)+\.?(:\d+)?(\/\S*)?)/g),
   mentionsREGEX = new RegExp(/\B@[a-z0-9_-]+/gi),
   emoticonREGEX = new RegExp(/\(([^)]+)\){1,10}$/g);
   
   //Split words or characters into individual characters
   var messageContentArray = messageContent.split(" ");
   
   //Loop over each individual string in message
   for(var i = messageContentArray.length - 1; i>=0; i--){
      
       var messageFragement = messageContentArray[i];
       
        /****************
         Emoticons
        *****************/
        if(messageFragement.match(emoticonREGEX) != null && messageFragement.length < 18){
            if(json_output.emoticons === undefined) //create array to store emoticon values if none exist yet
                json_output.emoticons = [];
            messageFragement = messageFragement.substring(1, messageFragement.length-1); //remove parenthesis
            json_output.emoticons.push(messageFragement);
        }
         
        /****************
         Mentions
        *****************/
        if(messageFragement.match(mentionsREGEX) != null){
            if(json_output.mentions === undefined) //create array to store mention values if none exist yet
                json_output.mentions = [];
            messageFragement = messageFragement.substr(1);//remove @ symbol
            json_output.mentions.push(messageFragement);
        }
    
        /****************
         URLs
        *****************/
        if(messageFragement.match(urlREGEX) != null){
            if(messageFragement.substring(0, 5) === "https" || messageFragement.substring(0, 4) === "http"){
                 URLs.push(messageFragement)
            }
            else{
                 messageFragement = "http://" + messageFragement; //http protocol required for URL to be retrieved
                 URLs.push(messageFragement)
            }
       }
       
       if(i === 0){
           processResults(); //process results after final loop iteration
       }
   }
   
   /****************
    Compile and Output Results
   *****************/
   function processResults(){
       
       function outputResults(){
            console.log(json_output);
            alert('Please check web console for resulting JSON string.');
        }
       
       if(URLs.length === 0){
           outputResults();
       }
       else{
           json_output.links = [];//create container for URL data
           var postData = {};//container used to send URL inside an object to server
        
            function getURLtitle(currentValue, index, array) {
                postData.messageURL = currentValue;
    
                $.ajax({
                    url: '/data',
                    type: "POST",
                    data: JSON.stringify(postData),
                    contentType: "application/json",
                    async: true,
                    success: function(data){
                        var completedURL = {};
                        completedURL.url = currentValue;
                        completedURL.title = data;
                        json_output.links.push(completedURL);
                        if(index === 0)
                            outputResults();
                    }
                });
            }
            
            URLs.forEach(getURLtitle);
     
       }
       
   }
   
});