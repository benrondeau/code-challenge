# Atlassian Code Challenge

Live URL: [https://atlassian-code-challenge.herokuapp.com/](https://atlassian-code-challenge.herokuapp.com/)

### Purpose
Process a string that contains emoticons, mentions, and URLs. After processing, a JSON object will be returned for each category as well as a URL title for each URL.

### URL Caveats
Given the hundreds of possible URLs, I only choose to support the most common types:

- https://www.google.com
- http://www.google.com
- https://google.com
- http://google.com
- www.google.com
- google.com
- 74.125.224.72 (IPv4 Address)

It is a wildcard what will return when a different URL is input.

### Output
After processing the input string, the JSON object is returned to the web console.