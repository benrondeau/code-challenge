var request = require("request"),
    express = require("express"),
    bodyParser = require("body-parser"),
    cheerio = require("cheerio");

var app = express();
app.use(bodyParser.json());
app.use(express.static(__dirname + '/assets')); //setup static public directory

//Homepage
app.get('/', function(req, res){
    res.sendfile('./assets/index.html');
});

// API for getting URL Titles
app.post('/data', function(req, res){
    
    request({
      uri: req.body.messageURL,
    },
    function(error, response, body) {
        
        var $ = cheerio.load(body);
        var title = $("title").text();
        
        res.send(title);
    });
});


//Server host/port
var host = (process.env.PORT);

//Keep the server open/listening
app.listen(host);
console.log('App started @ ' + process.env.IP);